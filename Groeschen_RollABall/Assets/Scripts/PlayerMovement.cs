﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerMovement : MonoBehaviour

{
    public Vector3 jump;
    public bool isGrounded;
    public bool isGameOver;
    public float jumpForce = 2.0f;
    public float speed;
    public float timeLeft = 45.0f;
    public Text countText;
    public Text winText;
    public Text lossText;
    public Text timerText;
    
    private Rigidbody rb;
    private int count;
    

    void Start()
    {
        rb = GetComponent<Rigidbody>();
        count = 0;
        SetCountText ();
        winText.text = "";
        lossText.text = "";
        jump = new Vector3(0.0f, 2.0f, 0.0f);
    }

    void FixedUpdate()
    {
        float moveHorizontal = Input.GetAxis("Horizontal");
        
        float moveVertical = Input.GetAxis("Vertical");

        Vector3 movement = new Vector3(moveHorizontal, 0.0f, moveVertical);
        
        rb.AddForce (movement * speed);


        if (Input.GetKey(KeyCode.LeftShift))
        {
            rb.AddForce(movement * speed * 2);
            
        }

        
        timeLeft -= Time.deltaTime;

        timerText.text = "Time Left: " + timeLeft.ToString("0");

        if (timeLeft <= 0)
        {
            timerText.text = "Time Left: 0";
            if (count <= 25)
            {
                isGameOver = true;
                lossText.text = "You Lose...";
            }
            
        }
    }

    void Update()
    {
        if(Input.GetKeyDown(KeyCode.Space) && isGrounded)
        {
            rb.AddForce(jump * jumpForce, ForceMode.Impulse);
            isGrounded = false;
        }

        if (Input.GetKeyDown(KeyCode.LeftShift))
        {
            GetComponent<Renderer>().material.color = Color.red;
        }

        if (Input.GetKeyUp(KeyCode.LeftShift))
        {
            GetComponent<Renderer>().material.color = Color.white;
        } 
    }

    void OnCollisionStay()
    {
        isGrounded = true;   
    }

    void OnCollisionExit()
    {
        isGrounded = false;
    }

    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Pickup") && isGameOver == false)
        {
            other.gameObject.SetActive(false);
            count = count + 1;
            SetCountText();
        }
    }

    void SetCountText ()
    {
        countText.text = "Count: " + count.ToString();
        if (count >= 26)
        {
            winText.text = "You Win!";
        }
    }
}
